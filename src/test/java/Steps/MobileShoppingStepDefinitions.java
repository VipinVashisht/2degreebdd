package Steps;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import Common.Browser;
import Common.Config;
import PageObject.HomePage;
import PageObject.PhonePage;
import PageObject.ShopPage;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MobileShoppingStepDefinitions {
	WebDriver driver;
	HomePage homepage;
	ShopPage shoppage;
	PhonePage phonepage;
	String phoneName;
	
	@Given("^I open (\\d+)Degrees web site$")
	public void i_open_2Degrees_web_site(int arg1) throws Throwable {
		try {
			driver=Browser.openBrowser();
			Browser.navigateToURL(driver, "homepage_prod");
			Assert.assertTrue(true);
		} catch (Exception e) {
			System.out.println("Could not navigate to homepage on Browser");
			Assert.assertTrue(false);
		}
	}

	@When("^I navigate to the 'Shop' page$")
	public void i_navigate_to_the_Shop_page() throws Throwable {
		HomePage homepage =new HomePage(driver);
		shoppage=homepage.clickShopLink(driver);
	}

	@Then("^I should see list of phones$")
	public void i_should_see_list_of_phones() throws Throwable {
		int numberOfPhones=shoppage.getListOfPhoneNames();
		//Verify that at least one phone exists
		Assert.assertTrue(numberOfPhones>0);
	}

	
    
    
    @Given("^I click on the phone \"([^\"]*)\"$")
    public void i_click_on_the_phone(String phoneName) throws Throwable {
    	this.phoneName=phoneName;
    	try {
			driver=Browser.openBrowser();
			Browser.navigateToURL(driver, "shoppage_prod");
			Assert.assertTrue(true);
		} catch (Exception e) {
			System.out.println("Could not navigate to Shop Page on Browser");
			Assert.assertTrue(false);
		}
    	ShopPage shoppage =new ShopPage(driver);
    	phonepage=shoppage.clickOnPhone(phoneName, driver);
	}
    

    @When("^I navigate to phone detail page$")
    public void i_navigate_to_phone_detail_page() throws Throwable {
        Assert.assertTrue(phonepage.verifyPhoneNameOnDetailPage(phoneName, driver));
    }

    @Then("^I should see total monthly cost as \"([^\"]*)\"$")
    public void i_should_see_total_monthly_cost_as(String totalCost) throws Throwable {
    	String actualCost=phonepage.getTotalValue(driver);
    	Assert.assertEquals(totalCost.trim(), actualCost.trim());
    }
    @After
    public void tearDown(){
    	Config.tearDownActivities(driver);
    }
    
}
