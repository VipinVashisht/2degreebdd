
package Common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

public class Config {
	
	Properties property = new Properties();
	
	/**
	 * Load Config file
	 */
	public void getTestEnvironmentInfo(){
			
			//Read the Config file
			String path = System.getProperty("user.dir") + "//src//main//Java//Parameters//Config.properties";
			System.out.println("Read the configuration file:-" + path.replace("//", "\\"));
			
			try {
				FileInputStream fn = new FileInputStream(path);
				property.load(fn);
				fn.close();
			} catch (IOException e) {
				System.out.println(e);
				}

			}

	/**
	 * Get the Config Property value
	 * @param key key name whose value is needed
	 * @return value of the specified key
	 */
	public Object getObjectProperty(String key)
	{
		getTestEnvironmentInfo();
		String keyName = key.toLowerCase();
		Object value = "";
		try
		{
			value = property.get(keyName);
			System.out.println("Reading key: " + keyName + " value:-'" + value + "'");
		}
		catch (Exception e)
		{
			System.out.println("'" + key + "' not found in Config Properties");
			org.junit.Assert.assertTrue(false);
			return null;
		}
		return value;
	}
	public static void tearDownActivities(WebDriver driver){
    	Browser.quitBrowser(driver);
    	//In realtime there may be other activities added here for teardown
    }
}
