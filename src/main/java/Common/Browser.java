package Common;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;


public class Browser {

	/** 
	 * Opens the new browser instance using the given config
	 * @return new browser instance
	 * @throws IOException 
	 */
	public static WebDriver openBrowser() throws IOException
	{
		Config config=new Config();
		String browserName=config.getObjectProperty("Browser").toString();
		System.out.println("Opening browser: "+browserName);
			
		WebDriver driver;	
		switch(browserName.toLowerCase())
			{
			case "firefox":
				System.out.println("user directory is "+System.getProperty("user.dir"));
				System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+File.separator+"lib"+File.separator+"geckodriver.exe");
				driver = new FirefoxDriver();								
				break;
			case "htmlunit":
				driver = new HtmlUnitDriver(true);
				break;
			case "chrome":
				driver = new ChromeDriver();
				break;
				
			default:
				System.out.println("Non Supported Browser:"+browserName);
				junit.framework.Assert.assertTrue(false);
				return null;
			}
			driver.manage().window().maximize();
			return driver;
	}

	
		/**
	 * Navigate to driver the URL specified
	 * @param driver Webdriver instance
	 * @param url URL to be navigated
	 */
	public static String navigateToURL(WebDriver driver, String urlKey)
	{
		Config config = new Config();
		String url =config.getObjectProperty(urlKey).toString();
		driver.navigate().to(url);
		System.out.println("Navigate to web page: '" + url + "'");
		return url;
	}

	/**
	 * Quits this driver, closing every associated window.
	 * 	@param driver Webdriver instance
	 */
	public static void quitBrowser(WebDriver driver)
	{
			System.out.println("Quit the browser");
			driver.quit();
		
	}
	
	/**
	 * @param driver: instance of webdriver
	 * @param element to be waited for
	 * @param description of the element
	 * @param seconds to be waited for
	 */
	public static void waitForPageLoad(WebDriver driver, WebElement element, String description, int seconds)
	{
		System.out.println("Wait for page load by waiting for element "+description);

		WebDriverWait wait= new WebDriverWait(driver, seconds);
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch(StaleElementReferenceException e)
		{
			wait.until(ExpectedConditions.visibilityOf(element));

		} 
	}

	/**Take screenshot
	 * @param driver: Wedriver instance
	 */
	public static void takeScreenshot(WebDriver driver){
		//create destination filename
	     DateFormat df = new SimpleDateFormat("dd_MM_yy-HH_mm_ss");
	     Date currentDate = new Date();       
	     File destination = new File(System.getProperty("user.dir")+File.separator+"screenshot"+File.separator+df.format(currentDate)+".png");
	     
	     try{
			File screenshot=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			 FileUtils.copyFile(screenshot, destination);
			 System.out.println("Screenshot saved as: "+destination);
	     }
	     catch(Exception e){
	    	 System.out.println("Screenshot could not be taken because "+e.getMessage());
	     }
	}
	
	
	}
