package Common;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;

public class Element {

	/**
	 * @param element WebElement to be clicked
	 * @param description logical name of specified WebElement, used for Logging purposes in report
	 */
	public static void click(WebElement element, String description)
	{
		System.out.println("Click on '" + description + "'");
		try 
		{
				element.click();	
		} 
		catch(StaleElementReferenceException e)
		{
			element.click();
		}
		catch (UnreachableBrowserException e) 
		{
			System.out.println("Browser is Unreachable");
			Assert.assertTrue(false);
		}
	}

	public static void enterData(WebElement element, String value, String description)
	{
			
			System.out.println("Enter the " + description + " as '" +  value + "'");
			element.clear();
			element.sendKeys(value);
		}
	
	/**
	 * @param element WebElement to be cleared
	 * @param description logical name of specified WebElement, used for Logging purposes in report
	 */
	public static void clear(WebElement element, String description)
	{
		System.out.println("Clear data of '" + description + "'");
		element.clear();
	}
	
	/**
	 * Wait for element to be visible on the page
	 * @param driver Webdriver instance
	 * @param element element to be searched
	 * @param description logical name of specified WebElement, used for Logging purposes in report
	 */
	public static void waitForVisibility( WebDriver driver, WebElement element, String description, int seconds)
	{
		System.out.println("Wait for element '" + description + "' to be visible on the page.");
		WebDriverWait wait= new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOf(element));
	}


	/**
	 * Wait for element to be visible on the page
	 * @param driver Webdriver instance
	 * @param element whose text is to be found
	 * @param description logical name of specified WebElement, used for Logging purposes in report
	 */
	public static String getText( WebDriver driver, WebElement element, String description)
	{
		String text=element.getText().trim();
		return  text;
		}

	/**
	 * Wait for element to be visible on the page
	 * @param driver Webdriver instance
	 * @param cssSelectorText of the element to be found
	 * @param description logical name of specified WebElement, used for Logging purposes in report
	 */
	public static WebElement findElement( WebDriver driver, String cssselectorText, String description)
	{
		WebElement element=null;
		try{
		element=driver.findElement(By.cssSelector(cssselectorText));
		System.out.println("Element '"+description+"' found");
		}
		catch(Exception e){
		//retry.fineline6260425 small.fineline:contains('I/N: 6260425')
		
		try{
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			element=driver.findElement(By.cssSelector(cssselectorText));
			}
		catch(Exception ex){
			System.out.println("Element '"+description+"'not found");
			Assert.assertTrue(false);
			}
		}
		return  element;
		}	
	}
