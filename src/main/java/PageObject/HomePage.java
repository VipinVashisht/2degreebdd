package PageObject;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Common.Browser;
import Common.Element;


public class HomePage {
	
	WebDriver driver;
	
	@FindBy (css="a[title='Shop'][class*='menu__item'")
	private  WebElement shopLink;

	//Constructor to verify all elements have loaded
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		Browser.waitForPageLoad(this.driver, shopLink, "shop link",30);
	}
	
	public  ShopPage clickShopLink(WebDriver driver)
	{
		Element.click(shopLink, "shop link");		
		return new ShopPage(driver);
	}
	

}
