package PageObject;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Common.Element;
import Common.Browser;

public class ShopPage {

		WebDriver driver;
	
		
		@FindBy (css=".itemgrid a")
		private WebElement firstPhone;
		
		//Constructor to verify all elements have loaded
		public ShopPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
			Browser.waitForPageLoad(this.driver, firstPhone, "First phone image",30);
		}
		
		public int getListOfPhoneNames() {
			WebElement phoneGrid=driver.findElement(By.className("itemgrid"));
			List<WebElement> phoneList=phoneGrid.findElements(By.cssSelector("a.item-container.itembox"));
			System.out.println("Phones in the list are "+ phoneList.size()+" in number ");
			return phoneList.size();
		}
		
		public PhonePage clickOnPhone(String phoneName, WebDriver driver) {
			WebElement phoneGrid=driver.findElement(By.className("itemgrid"));
			
			if(phoneName.contains("Samsung")) {
				phoneName=phoneName.replace("Samsung", "").trim();
			}
			
			List<WebElement> phoneList=phoneGrid.findElements(By.cssSelector("a.item-container.itembox"));
			Iterator<WebElement> iter = phoneList.iterator();
			while (iter.hasNext()) {
				WebElement item = iter.next();
				String phoneNameOnPage = item.findElement(By.tagName("h3")).getText();
				//System.out.println("Phone Name in list is: " + phoneNameOnPage);
				if(phoneNameOnPage.trim().equals(phoneName)){
					Element.click(item, phoneName);
					//not a good practice but due to lack of time, have not been able to find a stable condition, hence just putting it temporarily
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				}
				
				}
			return new PhonePage(driver);
			
			
		}
			
}
