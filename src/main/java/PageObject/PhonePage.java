package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Common.Browser;

public class PhonePage {
	WebDriver driver;
	
	@FindBy (css="section footer")
	private  WebElement phoneOptionGrid;

	@FindBy (css="header h1")
	private  WebElement phoneNameElement;

	
	//Constructor to verify all elements have loaded
		public PhonePage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
			Browser.waitForPageLoad(this.driver, phoneNameElement, "Phone title",30);
		}
		
		public String getTotalValue(WebDriver driver) {
			WebElement totalValue=phoneOptionGrid.findElement(By.cssSelector("h1"));
			System.out.println(totalValue.getText());
			return totalValue.getText();
		}
		
		public boolean verifyPhoneNameOnDetailPage(String phoneName, WebDriver driver) {
		String actualPhoneName=phoneNameElement.getText().trim();
		if(actualPhoneName.contains(phoneName)) {
			System.out.println("Found the phone name in the Shop page list");
			return true;
		}
		else {
			return false;
		}
		}
}