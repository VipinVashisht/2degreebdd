Feature: Select a phone from 2Degrees online shop

 Scenario: Navigating to the online shop page
   Given I open 2Degrees web site
   When I navigate to the 'Shop' page
   Then I should see list of phones

  Scenario Outline: Check the price of a phone
   Given I click on the phone "<phone>"
   When I navigate to phone detail page
   Then I should see total monthly cost as "$<total>"

   Examples:
     | phone                             | total  |
     | iPhone XS                        | 113.38 |
     | Samsung Galaxy S10               | 103.05 |
     | Find X Lamborghini Edition        | 101.38 |  