$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("VerifyMobileShopping.feature");
formatter.feature({
  "line": 1,
  "name": "Select a phone from 2Degrees online shop",
  "description": "",
  "id": "select-a-phone-from-2degrees-online-shop",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Navigating to the online shop page",
  "description": "",
  "id": "select-a-phone-from-2degrees-online-shop;navigating-to-the-online-shop-page",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "I open 2Degrees web site",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I navigate to the \u0027Shop\u0027 page",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "I should see list of phones",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 7
    }
  ],
  "location": "MobileShoppingStepDefinitions.i_open_2Degrees_web_site(int)"
});
formatter.result({
  "duration": 9027234032,
  "status": "passed"
});
formatter.match({
  "location": "MobileShoppingStepDefinitions.i_navigate_to_the_Shop_page()"
});
formatter.result({
  "duration": 3348883047,
  "status": "passed"
});
formatter.match({
  "location": "MobileShoppingStepDefinitions.i_should_see_list_of_phones()"
});
formatter.result({
  "duration": 33360514,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 8,
  "name": "Check the price of a phone",
  "description": "",
  "id": "select-a-phone-from-2degrees-online-shop;check-the-price-of-a-phone",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 9,
  "name": "I click on the phone \"\u003cphone\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I navigate to phone detail page",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should see total monthly cost as \"$\u003ctotal\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 13,
  "name": "",
  "description": "",
  "id": "select-a-phone-from-2degrees-online-shop;check-the-price-of-a-phone;",
  "rows": [
    {
      "cells": [
        "phone",
        "total"
      ],
      "line": 14,
      "id": "select-a-phone-from-2degrees-online-shop;check-the-price-of-a-phone;;1"
    },
    {
      "cells": [
        "iPhone XS",
        "113.38"
      ],
      "line": 15,
      "id": "select-a-phone-from-2degrees-online-shop;check-the-price-of-a-phone;;2"
    },
    {
      "cells": [
        "Samsung Galaxy S10",
        "103.05"
      ],
      "line": 16,
      "id": "select-a-phone-from-2degrees-online-shop;check-the-price-of-a-phone;;3"
    },
    {
      "cells": [
        "Find X Lamborghini Edition",
        "101.38"
      ],
      "line": 17,
      "id": "select-a-phone-from-2degrees-online-shop;check-the-price-of-a-phone;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 15,
  "name": "Check the price of a phone",
  "description": "",
  "id": "select-a-phone-from-2degrees-online-shop;check-the-price-of-a-phone;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 9,
  "name": "I click on the phone \"iPhone XS\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I navigate to phone detail page",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should see total monthly cost as \"$113.38\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "iPhone XS",
      "offset": 22
    }
  ],
  "location": "MobileShoppingStepDefinitions.i_click_on_the_phone(String)"
});
formatter.result({
  "duration": 13700269565,
  "status": "passed"
});
formatter.match({
  "location": "MobileShoppingStepDefinitions.i_navigate_to_phone_detail_page()"
});
formatter.result({
  "duration": 15720259,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$113.38",
      "offset": 36
    }
  ],
  "location": "MobileShoppingStepDefinitions.i_should_see_total_monthly_cost_as(String)"
});
formatter.result({
  "duration": 28099398,
  "error_message": "org.junit.ComparisonFailure: expected:\u003c$11[3].38\u003e but was:\u003c$11[1].38\u003e\r\n\tat org.junit.Assert.assertEquals(Assert.java:115)\r\n\tat org.junit.Assert.assertEquals(Assert.java:144)\r\n\tat Steps.MobileShoppingStepDefinitions.i_should_see_total_monthly_cost_as(MobileShoppingStepDefinitions.java:78)\r\n\tat ✽.Then I should see total monthly cost as \"$113.38\"(VerifyMobileShopping.feature:11)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 16,
  "name": "Check the price of a phone",
  "description": "",
  "id": "select-a-phone-from-2degrees-online-shop;check-the-price-of-a-phone;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 9,
  "name": "I click on the phone \"Samsung Galaxy S10\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I navigate to phone detail page",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should see total monthly cost as \"$103.05\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Samsung Galaxy S10",
      "offset": 22
    }
  ],
  "location": "MobileShoppingStepDefinitions.i_click_on_the_phone(String)"
});
formatter.result({
  "duration": 12335654225,
  "status": "passed"
});
formatter.match({
  "location": "MobileShoppingStepDefinitions.i_navigate_to_phone_detail_page()"
});
formatter.result({
  "duration": 13816309,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$103.05",
      "offset": 36
    }
  ],
  "location": "MobileShoppingStepDefinitions.i_should_see_total_monthly_cost_as(String)"
});
formatter.result({
  "duration": 27924720,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Check the price of a phone",
  "description": "",
  "id": "select-a-phone-from-2degrees-online-shop;check-the-price-of-a-phone;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 9,
  "name": "I click on the phone \"Find X Lamborghini Edition\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I navigate to phone detail page",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should see total monthly cost as \"$101.38\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Find X Lamborghini Edition",
      "offset": 22
    }
  ],
  "location": "MobileShoppingStepDefinitions.i_click_on_the_phone(String)"
});
formatter.result({
  "duration": 13650595836,
  "status": "passed"
});
formatter.match({
  "location": "MobileShoppingStepDefinitions.i_navigate_to_phone_detail_page()"
});
formatter.result({
  "duration": 13289358,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$101.38",
      "offset": 36
    }
  ],
  "location": "MobileShoppingStepDefinitions.i_should_see_total_monthly_cost_as(String)"
});
formatter.result({
  "duration": 25862868,
  "status": "passed"
});
});